package com.grievsie.approles;

import com.grievsie.approles.health.TemplateHealthCheck;
import com.grievsie.approles.resources.AppRolesResource;
import com.grievsie.approles.resources.HelloResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;


/**
 * The App Role web service - Application
 *
 * Created by Grievsie on 19/02/2016.
 */
public class AppRoleApplication extends Application<AppRoleConfiguration> {

    public static void main(String[] args) throws Exception {
        new AppRoleApplication().run(args);
    }

    @Override
    public String getName() {
        return "approle-dropwizard";
    }



    @Override
    public void initialize(Bootstrap<AppRoleConfiguration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(AppRoleConfiguration configuration, Environment environment) {

        final HelloResource helloResource = new HelloResource(
                configuration.getTemplate(),
                configuration.getDefaultName()
        );

        final AppRolesResource appRolesResource = new AppRolesResource(
                "Richard.Grieve",
                "Senior Development Manager",
                "Rick Grieve"
        );

        final TemplateHealthCheck healthCheck =
                new TemplateHealthCheck(configuration.getTemplate());

        environment.healthChecks().register("template", healthCheck);

        environment.jersey().register(helloResource);
        environment.jersey().register(appRolesResource);
    }
}
