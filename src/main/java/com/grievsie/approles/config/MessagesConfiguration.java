package com.grievsie.approles.config;

import javax.validation.constraints.NotNull;

/**
 * POJO representing messages in configuration.yml
 *
 * Created by Grievsie on 19/02/2016.
 */
public class MessagesConfiguration {

    @NotNull
    private String hello;

    @NotNull
    private String goodbye;

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }


    public String getGoodbye() {
        return goodbye;
    }

    public void setGoodbye(String goodbye) {
        this.goodbye = goodbye;
    }
}
