package com.grievsie.approles.config;

import io.dropwizard.Configuration;

import javax.validation.Valid;


/**
 * Represents configuration.yml.
 *
 * Does this by extending from DropWizard Configuration.
 *
 * If you want to put something in configuration.yml and access it then all you need to do is add some properties
 * to this class.
 *
 * Created by Grievsie on 19/02/2016.
 */
public class ServiceConfiguration extends Configuration {

    // The configuration is only valid if there is a messages section in the file
    @Valid
    private MessagesConfiguration messages;

    public MessagesConfiguration getMessages() {
        return messages;
    }

    public void setMessages(MessagesConfiguration messages) {
        this.messages = messages;
    }

}
