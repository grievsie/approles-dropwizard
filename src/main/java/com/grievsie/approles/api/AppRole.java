package com.grievsie.approles.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

/**
 * Created by Grievsie on 21/02/2016.
 */
public class AppRole {

    // Id for any requests using AppRole (see AppRolesResource for how it's incremented)
    private long id;

    @Length(max = 20)
    private String name;

    @Length(max = 50)
    private String description;

    @Length(max = 20)
    private String displayName;

    public AppRole() {
        // Jackson deserialisation
    }


    public AppRole(long id, String name, String description, String displayName) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.displayName = displayName;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public String getDescription() {
        return description;
    }

    @JsonProperty
    public String getDisplayName() {
        return displayName;
    }

}
