package com.grievsie.approles.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Optional;
import com.grievsie.approles.api.Saying;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;

/**
 * The Hello resource.
 *
 * GET /hello
 *
 * Created by Grievsie on 19/02/2016.
 */
@Path(value = "/hello")
@Produces(MediaType.APPLICATION_JSON)
public class HelloResource {
    private final String template;
    private final String defaultName;
    private final AtomicLong counter;

    public HelloResource(String template, String defaultName) {
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
    }


    @GET
    @Timed
    /**
     * @Timed - Dropwizard automatically records duration and rate of invocations as a Metrics Timer.
     *
     * @QueryParam("name") tells Jersey to map name param from query string to the name param in the method.
     * For example:
     * - client sends request to /hello?name=Rick, then sayHello with be called with Optional.of("Rick").
     * - client sends with no name param, then SayHello will be called with Optional.absent().
     */
    public Saying sayHello(@QueryParam("name") Optional<String> name) {

        // Format the template
        final String value = String.format(template, name.or(defaultName));

        // Increment counter and return a new Saying (This is what increments ID)
        return new Saying(counter.incrementAndGet(), value);
    }


}
