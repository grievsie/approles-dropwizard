package com.grievsie.approles.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Optional;
import com.grievsie.approles.api.AppRole;
import com.grievsie.approles.api.Saying;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Grievsie on 21/02/2016.
 */
@Path(value = "/approles")
@Produces(MediaType.APPLICATION_JSON)
public class AppRolesResource {

    private final String name;
    private final String description;
    private final String displayName;
    private final AtomicLong counter;

    public AppRolesResource(String name, String description, String displayName) {
        this.name = name;
        this.description = description;
        this.displayName = displayName;
        this.counter = new AtomicLong();
    }



    // TODO - will just return 1 AppRole for starters - will change to return AppRoles next
    public AppRole getAppRoles() {

        // TODO just returning a hardcoded approle for a min
        return new AppRole(counter.incrementAndGet(), name, description, displayName);
    }

}
