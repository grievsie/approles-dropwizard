package com.grievsie.approles.resources;


import com.grievsie.approles.config.MessagesConfiguration;

import javax.ws.rs.GET;
import javax.ws.rs.Path;


/**
 * Created by Grievsie on 19/02/2016.
 */

@Path(value = "/goodbye")
public class GoodbyeResource {
    private final MessagesConfiguration conf;

    public GoodbyeResource(MessagesConfiguration conf) {
        this.conf = conf;
    }

    @GET
    public String sayGoodbye() {
        return conf.getGoodbye();
    }

}
