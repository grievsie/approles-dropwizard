package com.grievsie.approles;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Our environment-specific properties!
 *
 * Comes from the configuration.yml file.
 *
 * Class is deserialised from the yml file.
 * The @JsonProperty stuff allows Jackson to serialise / deserialise the properties to/from the yml file.
 *
 *  Created by Grievsie on 20/02/2016.
 */
public class AppRoleConfiguration extends Configuration {

    @NotEmpty
    private String template;

    @NotEmpty
    private String defaultName = "Stranger";

    @JsonProperty
    public String getTemplate() {
        return template;
    }

    @JsonProperty
    public void setTemplate(String template) {
        this.template = template;
    }

    @JsonProperty
    public String getDefaultName() {
        return defaultName;
    }

    @JsonProperty
    public void setDefaultName(String name) {
        this.defaultName = name;
    }
}
